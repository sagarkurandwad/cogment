package cmd

import (
	"bytes"
	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConfigureCommand(t *testing.T) {
	viper.SetFs(afero.NewMemMapFs())
	initConfig()

	const expectedToken = "ABCD12345"

	var stdin bytes.Buffer
	stdin.Write([]byte(expectedToken + "\n"))

	err := runConfigureCmd(&stdin)

	if err := viper.ReadInConfig(); err != nil {
		t.Fatal("Unable to read config file : ", err)
	}

	assert.Nil(t, err)
	assert.Equal(t, expectedToken, viper.GetString("default.token"))
	assert.Equal(t, defaultApiUrl, viper.GetString("default.url"))
}
