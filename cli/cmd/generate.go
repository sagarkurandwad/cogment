/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/cogment/cogment/api"
	"gitlab.com/cogment/cogment/templates"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

const SETTINGS_FILENAME = "cog_settings"
const LANG_PYTHON = 0
const LANG_JAVASCRIPT = 1

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate settings and compile your proto files",
	PreRun: func(cmd *cobra.Command, args []string) {
		if !cmd.Flags().Changed("python_dir") && !cmd.Flags().Changed("js_dir") {
			log.Fatalln("no destination specified")
		}

		file, err := cmd.Flags().GetString("file")
		if err != nil {
			log.Fatalln(err)
		}

		if _, err := os.Stat(file); os.IsNotExist(err) {
			log.Fatalf("%s doesn't exist", file)
		}

	},
	Run: func(cmd *cobra.Command, args []string) {
		if err := runGenerateCmd(cmd); err != nil {
			log.Fatalln(err)
		}

		fmt.Println("Files have been generated")
	},
}

func runGenerateCmd(cmd *cobra.Command) error {

	file, err := cmd.Flags().GetString("file")
	if err != nil {
		log.Fatalln(err)
	}

	//TODO: validate logic in config

	src := filepath.Dir(file)

	if cmd.Flags().Changed("python_dir") {
		dest, err := cmd.Flags().GetString("python_dir")
		if err != nil {
			return err
		}
		config := CreateProjectConfigFromYaml(file)
		if err := generatePythonSettings(config, src, dest); err != nil {
			return err
		}
	}

	if cmd.Flags().Changed("js_dir") {
		dest, err := cmd.Flags().GetString("js_dir")
		if err != nil {
			return err
		}
		config := CreateProjectConfigFromYaml(file)
		if err := generateJavascriptSettings(config, src, dest); err != nil {
			return err
		}
	}

	return nil
}

func generatePythonSettings(config *api.ProjectConfig, src, dir string) error {
	dest := path.Join(dir, SETTINGS_FILENAME+".py")
	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}

	var err error
	for k, proto := range config.Import.Proto {
		config.Import.Proto[k] = strings.TrimSuffix(proto, ".proto") + "_pb2"
		if Verbose {
			log.Println("Compiling " + proto)
		}

		if err = compileProto(src, dir, proto, LANG_PYTHON); err != nil {
			break
		}
	}

	if err != nil {
		return err
	}

	for k, actorClass := range config.ActorClasses {
		if actorClass.ConfigType != "" {
			config.ActorClasses[k].ConfigType = lookupMessageType(actorClass.ConfigType, config.Import.Proto)
		}

		config.ActorClasses[k].Action.Space = lookupMessageType(actorClass.Action.Space, config.Import.Proto)
		config.ActorClasses[k].Observation.Space = lookupMessageType(actorClass.Observation.Space, config.Import.Proto)
		if actorClass.Observation.Delta != "" {
			config.ActorClasses[k].Observation.Delta = lookupMessageType(actorClass.Observation.Delta, config.Import.Proto)
		}
	}

	if config.Environment != nil && config.Environment.ConfigType != "" {
		config.Environment.ConfigType = lookupMessageType(config.Environment.ConfigType, config.Import.Proto)
	}

	if config.Trial != nil && config.Trial.ConfigType != "" {
		config.Trial.ConfigType = lookupMessageType(config.Trial.ConfigType, config.Import.Proto)
	}

	err = generateFromTemplate(templates.COG_SETTINGS_PY, dest, config)

	return err
}

func generateJavascriptSettings(config *api.ProjectConfig, src, dir string) error {
	dest := path.Join(dir, SETTINGS_FILENAME+".js")
	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}

	var err error
	for k, proto := range config.Import.Proto {
		config.Import.Proto[k] = strings.TrimSuffix(proto, ".proto") + "_pb"
		if Verbose {
			log.Println("Compiling " + proto)
		}

		if err = compileProto(src, dir, proto, LANG_JAVASCRIPT); err != nil {
			break
		}
	}

	if err != nil {
		return err
	}

	for k, actorClass := range config.ActorClasses {
		if actorClass.ConfigType != "" {
			config.ActorClasses[k].ConfigType = lookupMessageType(actorClass.ConfigType, config.Import.Proto)
		}

		config.ActorClasses[k].Action.Space = lookupMessageType(actorClass.Action.Space, config.Import.Proto)
		config.ActorClasses[k].Observation.Space = lookupMessageType(actorClass.Observation.Space, config.Import.Proto)
		config.ActorClasses[k].Observation.Delta = lookupMessageType(actorClass.Observation.Delta, config.Import.Proto)
	}

	config.Environment.ConfigType = lookupMessageType(config.Environment.ConfigType, config.Import.Proto)
	config.Trial.ConfigType = lookupMessageType(config.Trial.ConfigType, config.Import.Proto)

	err = generateFromTemplate(templates.COG_SETTINGS_JS, dest, config)

	return err
}

func CreateProjectConfigFromYaml(filename string) *api.ProjectConfig {

	yamlFile, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}

	config := api.ProjectConfig{}
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	//fmt.Println(helper.PrettyPrint(m))
	return &config
}

func lookupMessageType(name string, protos []string) string {
	s := strings.Split(name, ".")
	protoFile := findFileContainingSymbol(protos)

	return protoFile + "." + s[len(s)-1]
}

func findFileContainingSymbol(protos []string) string {
	// TODO: implement logic. Now return always the 1st proto
	return protos[0]
}

func compileProto(src, dest, proto string, language int8) error {

	var params []string
	if language == LANG_PYTHON {
		params = append(params, "-I"+src, "--python_out="+dest, path.Join(src, proto))
	} else if language == LANG_JAVASCRIPT {
		params = append(params, "-I"+src, "--js_out=import_style=commonjs,binary:"+dest, path.Join(src, proto))
	} else {
		return fmt.Errorf("language %d is not supported", language)
	}

	cmd := exec.Command("protoc", params...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		if Verbose {
			log.Println(cmd.String())
			log.Printf("combined out:\n%s\n", string(out))
			log.Printf("cmd.Run() failed with %s\n", err)
		}
	}

	return nil
}

//func CreateProjectFromYaml(filename string) *api.ProjectConfig {
//compose := filterServicesFromCompose(filename)
//
//m := NewDeploymentManifest()
//
//for name, svc := range compose.Services {
//
//	s := NewService()
//
//	//fmt.Println(prettyPrint(svc))
//
//	// Parse environment
//	for k, v := range svc.XCogment.Environment {
//		s.Environment[k] = v
//	}
//
//	// Parse ports
//	for _, v := range svc.XCogment.Ports {
//		s.Ports = append(s.Ports, &PortDefinition{
//			Port:   v.Port,
//			Public: v.Public,
//		})
//	}
//
//	// Parse image
//	image := svc.Image
//	if image == "" {
//		log.Fatalf("Service %s must define an image value in docker-compose", name)
//	}
//	s.Image = image
//
//	if len(svc.Build) > 0 {
//		s.setPushImage(true)
//	}
//
//	m.Services[name] = s
//}

//fmt.Println(helper.PrettyPrint(m))
//return m
//}

func init() {
	rootCmd.AddCommand(generateCmd)

	generateCmd.Flags().StringP("file", "f", "cogment.yaml", "project configuration file")
	generateCmd.Flags().String("python_dir", "", "destination of python generated files")
	generateCmd.Flags().String("js_dir", "", "destination of javascript generated files")

}
