package orchestrator

const DOCKERFILE = `
FROM registry.gitlab.com/cogment/cogment/orchestrator:0.2.1-beta

ADD cogment.yaml .
`
