package templates

const ROOT_COGMENT_YAML = `
import:
  proto:
    - data.proto

# Static configuration
actor_classes:
{{- range .ActorClasses}}
  - id: {{.Id|snakeify}}
    action:
      space: bootstrap.{{.Id|pascalify}}Action
    observation:
      space: bootstrap.Observation
{{end}}

# Dynamic configuration (could be changed by a pre-hook)
trial_params:
  environment:
    endpoint: grpc://env:9000

  actors:
  {{- range .TrialParams.Actors}}
    - actor_class: {{.ActorClass|snakeify}}
      endpoint: {{.Endpoint}}
  {{ end }}
`
