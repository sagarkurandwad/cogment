package envs

const MAIN_PY = `
import cog_settings
from data_pb2 import Observation

from cogment import Environment, GrpcServer


class Env(Environment):
    VERSIONS = {"env": "1.0.0"}

    def start(self, config):
        print("environment starting")
        observation = Observation()
        return observation

    def update(self, actions):
        print("environment updating")
        return Observation()

    def end(self):
        print("environment end")


if __name__ == "__main__":
    server = GrpcServer(Env, cog_settings)
    server.serve()
`
