package templates

const ROOT_README = `
This is a sample bootstraping project for the Cogment python SDK.

Clone this repository, and you should be good to go.

## Useful commands:

Compile the cogment.yaml and proto files:

    cogment --file /data/cogment.yaml --python_dir /data 

Launch the environment, agent and trial servers:

    docker-compose up orchestrator agent env

Run the client application:

    docker-compose run client
`
