// This code was generated by the easy_grpc protoc plugin.

#include "agent.egrpc.pb.h"

namespace aom_api {

// ********** Agent ********** //

const char* Agent::kAgent_Start_name = "/aom_api.Agent/Start";
const char* Agent::kAgent_Decide_name = "/aom_api.Agent/Decide";
const char* Agent::kAgent_Reward_name = "/aom_api.Agent/Reward";
const char* Agent::kAgent_Version_name = "/aom_api.Agent/Version";

Agent::Stub::Stub(::easy_grpc::client::Channel* c, ::easy_grpc::Completion_queue* default_queue)
  : channel_(c), default_queue_(default_queue ? default_queue : c->default_queue())
  , Start_tag_(c->register_method(kAgent_Start_name))
  , Decide_tag_(c->register_method(kAgent_Decide_name))
  , Reward_tag_(c->register_method(kAgent_Reward_name))
  , Version_tag_(c->register_method(kAgent_Version_name)) {}

// Start
::easy_grpc::Future<::aom_api::AgentStartReply> Agent::Stub::Start(::aom_api::AgentStartRequest req, ::easy_grpc::client::Call_options options) {
  if(!options.completion_queue) { options.completion_queue = default_queue_; }
  return ::easy_grpc::client::start_unary_call<::aom_api::AgentStartReply>(channel_, Start_tag_, std::move(req), std::move(options));
}

// Decide
::easy_grpc::Future<::aom_api::AgentDecideReply> Agent::Stub::Decide(::aom_api::AgentDecideRequest req, ::easy_grpc::client::Call_options options) {
  if(!options.completion_queue) { options.completion_queue = default_queue_; }
  return ::easy_grpc::client::start_unary_call<::aom_api::AgentDecideReply>(channel_, Decide_tag_, std::move(req), std::move(options));
}

// Reward
::easy_grpc::Future<::aom_api::AgentRewardReply> Agent::Stub::Reward(::aom_api::AgentRewardRequest req, ::easy_grpc::client::Call_options options) {
  if(!options.completion_queue) { options.completion_queue = default_queue_; }
  return ::easy_grpc::client::start_unary_call<::aom_api::AgentRewardReply>(channel_, Reward_tag_, std::move(req), std::move(options));
}

// Version
::easy_grpc::Future<::aom_api::VersionInfo> Agent::Stub::Version(::aom_api::VersionRequest req, ::easy_grpc::client::Call_options options) {
  if(!options.completion_queue) { options.completion_queue = default_queue_; }
  return ::easy_grpc::client::start_unary_call<::aom_api::VersionInfo>(channel_, Version_tag_, std::move(req), std::move(options));
}

} // namespaceaom_api

