#include "easy_grpc/easy_grpc.h"
#include "easy_grpc_reflection/reflection.h"

namespace rpc = easy_grpc;

#include "aom/config_file.h"
#include "aom/datalog/storage_interface.h"
#include "aom/orch_config.h"
#include "aom/orchestrator.h"
#include "aom/trial_params.h"
#include "aom/trial_spec.h"

#include <prometheus/exposer.h>
#include <prometheus/registry.h>

#include "slt/settings.h"
#include "spdlog/spdlog.h"

#include "yaml-cpp/yaml.h"

#include <csignal>
#include <thread>

namespace settings {
slt::Setting trial_port =
    slt::Setting_builder<std::uint16_t>()
        .with_default(9000)
        .with_description("The port to listen for trials on")
        .with_env_variable("PORT")
        .with_arg("port");

slt::Setting config_file = slt::Setting_builder<std::string>()
                               .with_default("cogment.yaml")
                               .with_description("The trial configuration file")
                               .with_arg("config");

slt::Setting prometheus_port =
    slt::Setting_builder<std::uint16_t>()
        .with_default(8080)
        .with_description("The port to broadcast prometheus metrics on")
        .with_env_variable("PROMETHEUS_PORT")
        .with_arg("prometheus_port");

slt::Setting display_version =
    slt::Setting_builder<bool>()
        .with_default(false)
        .with_description("Display the orchestrator's version and quit")
        .with_arg("version");
}  // namespace settings

int main(int argc, const char* argv[]) {
  slt::Settings_context ctx("aom_orchestrator", argc, argv);

  if (ctx.help_requested()) {
    return 0;
  }

  if (settings::display_version.get()) {
    // This is used by the build process to tag docker images reliably.
    // It should remaing this terse.
    std::cout << AOM_ORCHESTRATOR_VERSION << "\n";
    return 0;
  }

  ctx.validate_all();

  auto trial_config = YAML::LoadFile(settings::config_file.get());

  rpc::Environment grpc_env;

  std::array<rpc::Completion_queue, 4> server_queues;

  // ******************* Dependant services *******************
  spdlog::info("AoM Orchestrator v. {}", AOM_ORCHESTRATOR_VERSION);

  // ******************* Endpoints *******************
  auto trial_endpoint = fmt::format("0.0.0.0:{}", settings::trial_port.get());
  auto prometheus_endpoint =
      fmt::format("0.0.0.0:{}", settings::prometheus_port.get());

  // ******************* Monitoring *******************
  spdlog::trace("starting prometheus");
  prometheus::Exposer ExposePublicParser(prometheus_endpoint);

  // ******************* Datalog *******************
  std::string datalog_type = "none";
  if (trial_config["datalog"] && trial_config["datalog"]["type"]) {
    datalog_type = trial_config["datalog"]["type"].as<std::string>();
  }
  auto datalog =
      aom::Datalog_storage_interface::create(datalog_type, trial_config);

  // ******************* Server *******************
  aom::Trial_spec trial_spec(trial_config);
  auto params = aom::load_params(trial_config, trial_spec);

  aom::Orchestrator orchestrator(std::move(trial_spec), std::move(params),
                                 std::move(datalog));

  aom::Stub_pool<cogment::TrialHooks> hook_stubs(orchestrator.channel_pool(),
                                                 orchestrator.client_queue());
  std::vector<std::shared_ptr<aom::Stub_pool<cogment::TrialHooks>::Entry>>
      hooks;

  if (trial_config["trial"]) {
    for (auto hook : trial_config["trial"]["pre_hooks"]) {
      hooks.push_back(hook_stubs.get_stub(hook.as<std::string>()));
      orchestrator.add_prehook(&hooks.back()->stub);
    }
  }

  rpc::server::Config cfg;
  cfg.add_default_listening_queues({server_queues.begin(), server_queues.end()})
      .add_feature(easy_grpc::Reflection_feature())
      .add_service(orchestrator)
      .add_listening_port(trial_endpoint);

  spdlog::trace("building server");
  rpc::server::Server server(std::move(cfg));

  spdlog::info("Server listening for trials on {}", trial_endpoint);

  // Wait for a termination signal.
  sigset_t sig_set;
  sigemptyset(&sig_set);
  sigaddset(&sig_set, SIGINT);
  sigaddset(&sig_set, SIGTERM);

  // Prevent normal handling of these signals.
  pthread_sigmask(SIG_BLOCK, &sig_set, nullptr);

  int sig = 0;
  sigwait(&sig_set, &sig);

  spdlog::info("Shutting down...");

  return 0;
}