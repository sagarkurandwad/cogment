#ifndef AOM_ORCHESTRATOR_TEST_ORCHESTRATOR_FIXTURE_H
#define AOM_ORCHESTRATOR_TEST_ORCHESTRATOR_FIXTURE_H

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "mock_env.h"
#include "mock_agent.h"
#include "aom/orchestrator.h"

namespace rpc = easy_grpc;

template<typename T>
rpc::Future<T> ready_future(T v = {}) {
  rpc::Promise<T> prom;
  auto result = prom.get_future();
  prom.set_value(std::move(v));
  return result;
}

template<typename T>
rpc::Future<T> failed_future() {
  rpc::Promise<T> prom;
  auto result = prom.get_future();
  prom.set_exception(std::make_exception_ptr(std::runtime_error("forced")));
  return result;
}


class TestDataLogger : public aom::Datalog_storage_interface {
  std::vector<cogment::DatalogSample>& dst_;
public:
  class TestTrialLog : public aom::Trial_log_interface {
    std::vector<cogment::DatalogSample>& dst_;
  public:
    TestTrialLog(std::vector<cogment::DatalogSample>& dst) : dst_(dst) {}
    void add_sample(cogment::DatalogSample data) {
      dst_.push_back(data);
    }
  };

  TestDataLogger(std::vector<cogment::DatalogSample>& dst) : dst_(dst) {}
  std::unique_ptr<aom::Trial_log_interface> begin_trial(const uuids::uuid& trial_id) override {
    return std::make_unique<TestTrialLog>(dst_);
  }
};

// General purpose test fixture for the orchestrator
class Orchestrator_test : public ::testing::Test {
 protected:
   std::vector<cogment::DatalogSample> datalog;
  
  rpc::Environment grpc_env;

  aom::Mock_env env;
  std::shared_ptr<aom::Mock_agent> agent;

  std::unique_ptr<aom::Orchestrator> orchestrator;
  std::unique_ptr<std::array<rpc::Completion_queue, 1>> server_queues;
  std::unique_ptr<rpc::Completion_queue> client_queue;

  void SetUp() override {
    agent = std::make_shared<aom::Mock_agent>();
    client_queue = std::make_unique<rpc::Completion_queue>();
    server_queues = std::make_unique<std::array<rpc::Completion_queue, 1>>();


    std::vector<aom::ActorClass> ac(1);
    ac[0].name = "actor";
    
    orchestrator = std::make_unique<aom::Orchestrator>(
      env, 
      aom::ActorSpec(std::move(ac)),
      aom::Trial_config{}, 
      std::make_unique<TestDataLogger>(datalog));
  }

  void TearDown() override {
    orchestrator.reset();
    client_queue.reset();
    server_queues.reset();
    agent.reset();
  }

  ::cogment::TrialEndReply end_trial(const ::cogment::TrialStartReply& start) {
    using testing::Return;
    using testing::_;
    using testing::ByMove;

    ::cogment::TrialEndRequest req;
    req.set_trial_id(start.trial_id());

    EXPECT_CALL(env, End(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future<cogment::EnvEndReply>())));

    EXPECT_CALL(*agent, End(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future<cogment::AgentEndReply>())));

    return orchestrator->End(req);
  }

  ::cogment::TrialStartReply start_trial(std::string uid= "abc") {
    using testing::Return;
    using testing::_;
    using testing::ByMove;

    cogment::EnvStartReply env_start_rep;
    //One single absolute observation that goes to both actors...
    auto observation = env_start_rep.mutable_observation_set()->add_observations();

    observation->set_snapshot(true);
    observation->set_content("init");

    env_start_rep.mutable_observation_set()->add_actors_map(0);
    env_start_rep.mutable_observation_set()->add_actors_map(0);

    cogment::AgentStartReply agent_start_rep;
    cogment::AgentDecideReply agent_decide_rep;

    EXPECT_CALL(env, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future(env_start_rep))));

    EXPECT_CALL(*agent, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future(agent_start_rep))));

    EXPECT_CALL(*agent, Decide(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future(agent_decide_rep))));

    ::cogment::TrialStartRequest req;
    req.set_user_id(uid);
    
    auto rep = orchestrator->Start(req);

    return rep.get();
  }
};



class Orchestrator_test_no_human : public ::testing::Test {
 protected:
  rpc::Environment grpc_env;

  aom::Mock_env env;
  std::shared_ptr<aom::Mock_agent> agent;

  std::unique_ptr<aom::Orchestrator> orchestrator;
  std::unique_ptr<std::array<rpc::Completion_queue, 1>> server_queues;
  std::unique_ptr<rpc::Completion_queue> client_queue;

  void SetUp() override {
    agent = std::make_shared<aom::Mock_agent>();
    client_queue = std::make_unique<rpc::Completion_queue>();
    server_queues = std::make_unique<std::array<rpc::Completion_queue, 1>>();


    std::vector<aom::ActorClass> ac(1);
    ac[0].name = "actor";
   
    orchestrator = std::make_unique<aom::Orchestrator>(env, aom::ActorSpec(std::move(ac)), aom::Trial_config{}, nullptr);
  }

  void TearDown() override {
    orchestrator.reset();
    client_queue.reset();
    server_queues.reset();
    agent.reset();
  }


  ::cogment::TrialStartReply start_trial() {
    using testing::Return;
    using testing::_;
    using testing::ByMove;

    cogment::EnvStartReply env_start_rep;
    //One single absolute observation that goes to both actors...
    auto observation = env_start_rep.mutable_observation_set()->add_observations();
    observation->set_snapshot(true);
    observation->set_content("");

    env_start_rep.mutable_observation_set()->add_actors_map(0);
    env_start_rep.mutable_observation_set()->add_actors_map(0);

    cogment::AgentStartReply agent_start_rep;
    cogment::AgentDecideReply agent_decide_rep;

    EXPECT_CALL(env, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future(env_start_rep))));

    EXPECT_CALL(*agent, Start(_,_))
      .Times(2)
      .WillOnce(Return(ByMove(ready_future(agent_start_rep))))
      .WillOnce(Return(ByMove(ready_future(agent_start_rep))));

    EXPECT_CALL(*agent, Decide(_,_))
      .Times(2)
      .WillOnce(Return(ByMove(ready_future(agent_decide_rep))))
      .WillOnce(Return(ByMove(ready_future(agent_decide_rep))));

    ::cogment::TrialStartRequest req;
    auto rep = orchestrator->Start(req);

    return rep.get();
  }
};
#endif