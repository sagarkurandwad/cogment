#ifndef AOM_ORCHESTRATOR_TEST_MOCK_ENV_H
#define AOM_ORCHESTRATOR_TEST_MOCK_ENV_H

#include "gmock/gmock.h"
#include "cogment/api/environment.egrpc.pb.h"

namespace aom {
  class Mock_env : public cogment::Environment::Stub_interface {
    public:
      virtual ~Mock_env() {}
      MOCK_METHOD2(Start, ::easy_grpc::Future<::cogment::EnvStartReply>(::cogment::EnvStartRequest, ::easy_grpc::client::Call_options));
      MOCK_METHOD2(End, ::easy_grpc::Future<::cogment::EnvEndReply>(::cogment::EnvEndRequest, ::easy_grpc::client::Call_options));
      MOCK_METHOD2(Update, ::easy_grpc::Future<::cogment::EnvUpdateReply>(::cogment::EnvUpdateRequest, ::easy_grpc::client::Call_options));
      MOCK_METHOD2(Version, ::easy_grpc::Future<::cogment::VersionInfo>(::cogment::VersionRequest, ::easy_grpc::client::Call_options));
  };
}

#endif