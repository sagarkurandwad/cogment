#ifndef AOM_ORCHESTRATOR_TEST_MOCK_AGENT_H
#define AOM_ORCHESTRATOR_TEST_MOCK_AGENT_H

#include "gmock/gmock.h"
#include "cogment/api/agent.egrpc.pb.h"

namespace aom {
  class Mock_agent : public cogment::Agent::Stub_interface {
    public:
      virtual ~Mock_agent() {}
      MOCK_METHOD2(Start, ::easy_grpc::Future<::cogment::AgentStartReply>(::cogment::AgentStartRequest, ::easy_grpc::client::Call_options));
      MOCK_METHOD2(End, ::easy_grpc::Future<::cogment::AgentEndReply>(::cogment::AgentEndRequest, ::easy_grpc::client::Call_options));
      MOCK_METHOD2(Decide, ::easy_grpc::Future<::cogment::AgentDecideReply>(::cogment::AgentDecideRequest, ::easy_grpc::client::Call_options));
      MOCK_METHOD2(Reward, ::easy_grpc::Future<::cogment::AgentRewardReply>(::cogment::AgentRewardRequest, ::easy_grpc::client::Call_options));
      MOCK_METHOD2(Version  , ::easy_grpc::Future<::cogment::VersionInfo>(::cogment::VersionRequest, ::easy_grpc::client::Call_options));
  };
}

#endif
