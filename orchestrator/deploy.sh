#!/bin/bash

IMAGE=$1

TAG=$(docker run --rm $IMAGE:latest aom_orchestrator --version)

echo "newly built image wants to have the tag: $TAG."

docker pull $IMAGE:$TAG
HAS_PREV_IMAGE=$?

if [ $HAS_PREV_IMAGE -eq 0 ]
then
  echo "tag already in repository, checking to make sure it's the same image."
  docker save $IMAGE:$TAG | sha256sum -c _images/orchestrator.hash
  CHECK_TEST=$?
  if [ $CHECK_TEST -eq 0 ]
  then
    echo "new image is identical to that on repo. Nothing to do."
    exit 0
  else
    echo "newly built image differs from version in repo, despite having the same tag."
    exit 1
  fi
else
  echo "Tag is not on repository, performing push..."
  docker tag $IMAGE:latest $IMAGE:$TAG
  docker push $IMAGE:$TAG
  docker push $IMAGE:latest
  exit 0
fi