#ifndef AOM_BASE_64_INCLUDED_H
#define AOM_BASE_64_INCLUDED_H

#include <string>

namespace aom {
  std::string base64_encode(const std::string& data);
}

#endif