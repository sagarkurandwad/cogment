#include "aom/datalog/csv_storage.h"

#include "aom/base64.h"
#include "spdlog/spdlog.h"

#include <stdexcept>

namespace aom {

Local_csv_storage::Trial_log::Trial_log(Local_csv_storage* owner,
                                        const uuids::uuid&)
    : owner_(owner) {}

Local_csv_storage::Trial_log::~Trial_log() {}

void Local_csv_storage::Trial_log::add_sample(cogment::DatalogSample data) {
  owner_->send(std::move(data));
}

Local_csv_storage::Local_csv_storage(const std::string& file_name)
    : file_(file_name, std::ios::binary | std::ofstream::app),
      flush_pool(1, 1) {
  spdlog::info("writing csv datalog to: {}", file_name);
  if (!file_.good()) {
    throw std::runtime_error(
        std::string("failed to open csv datalog for writing: ") + file_name);
  }
}

Local_csv_storage::~Local_csv_storage() {}

std::unique_ptr<Trial_log_interface> Local_csv_storage::begin_trial(
    const uuids::uuid& trial_id) {
  return std::make_unique<Local_csv_storage::Trial_log>(this, trial_id);
}

void Local_csv_storage::send(cogment::DatalogSample sample) {
  flush_pool.push_job([this, s{std::move(sample)}]() {
    base64_encode(s.SerializeAsString());
    /*
    int actor_count = s.actions_size();
    assert(actor_count == s.rewards_size());


        std::string agent_action = base64_encode(s.agent_action());
        std::string user_action = base64_encode(s.user_action());

        std::string checkpoint;
        std::string delta;
        std::string time_stamp;

        if (s.has_time_stamp()) {
          time_stamp = base64_encode(s.time_stamp().SerializeAsString());
        }

        if (s.state_encoding() == cogment::DatalogSample::CHECKPOINT) {
          checkpoint = base64_encode(s.state());
          delta = "~";
        } else {
          delta = base64_encode(s.state());
          checkpoint = "~";
        }

        file_ << fmt::format("{}, {}, {}, {}, {}, {}, {}, {}\n", s.trial_id(),
                             time_stamp, checkpoint, delta, user_action,
                             agent_action, s.user_reward(), s.agent_reward());
        file_.flush();
        */
  });
}
}  // namespace aom