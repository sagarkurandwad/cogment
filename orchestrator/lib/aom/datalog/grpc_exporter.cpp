#include "aom/datalog/grpc_exporter.h"

#include "spdlog/spdlog.h"

#include <stdexcept>

namespace aom {

Grpc_datalog_exporter_base::Trial_log::Trial_log(Grpc_datalog_exporter_base* owner,
                                        const uuids::uuid&)
    : owner_(owner) {}

Grpc_datalog_exporter_base::Trial_log::~Trial_log() {}

void Grpc_datalog_exporter_base::Trial_log::add_sample(cogment::DatalogSample data) {
  owner_->send(std::move(data));
}

void Grpc_datalog_exporter_base::Trial_log::add_samples(std::vector<cogment::DatalogSample>&& data) {
  owner_->send(std::move(data));
}

std::unique_ptr<Trial_log_interface> Grpc_datalog_exporter_base::begin_trial(
    const uuids::uuid& trial_id) {
  return std::make_unique<Grpc_datalog_exporter::Trial_log>(this, trial_id);
}

void Grpc_datalog_exporter_base::send(std::vector<cogment::DatalogSample>&& samples) {
  auto [stream, reply] = stub_->Log();


  auto end = std::prev(samples.end());

  for (auto i = samples.begin(); i != end; ++i) {
    stream.push(std::move(*i));
  }

  stream.complete();
  reply.finally([](auto){});
}
 
void Grpc_datalog_exporter_base::send(cogment::DatalogSample sample) {
  auto [stream, reply] = stub_->Log();

  reply.finally([](auto){});
  spdlog::info("pung");
  
  stream.push(std::move(sample));
  stream.complete();  
}


Grpc_datalog_exporter::Grpc_datalog_exporter(const std::string& url) 
  : channel(url, &work_thread)
  , stub_impl(&channel) {
  set_stub(&stub_impl);
  
  spdlog::info("sending datalog to service running at: {}", url);
}

}  // namespace aom