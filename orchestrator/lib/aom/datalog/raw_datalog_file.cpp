#include "aom/datalog/raw_datalog_file.h"

#include "spdlog/spdlog.h"

#include <stdexcept>

namespace aom {

Raw_datalog_file_Storage::Trial_log::Trial_log(Raw_datalog_file_Storage* owner,
                                        const uuids::uuid&)
    : owner_(owner) {}

Raw_datalog_file_Storage::Trial_log::~Trial_log() {}

void Raw_datalog_file_Storage::Trial_log::add_sample(cogment::DatalogSample data) {
  owner_->send(std::move(data));
}

Raw_datalog_file_Storage::Raw_datalog_file_Storage(const std::string& file_name)
    : file_(file_name, std::ios::binary | std::ofstream::app),
      flush_pool(1, 1) {
  spdlog::info("writing raw proto datalog to: {}", file_name);
  if (!file_.good()) {
    throw std::runtime_error(
        std::string("failed to open raw proto datalog for writing: ") + file_name);
  }
}

Raw_datalog_file_Storage::~Raw_datalog_file_Storage() {}

std::unique_ptr<Trial_log_interface> Raw_datalog_file_Storage::begin_trial(
    const uuids::uuid& trial_id) {
  return std::make_unique<Raw_datalog_file_Storage::Trial_log>(this, trial_id);
}

void Raw_datalog_file_Storage::send(cogment::DatalogSample sample) {
  flush_pool.push_job([this, s{std::move(sample)}]() {

    auto data = s.SerializeAsString();
    auto pos = file_.tellp();

    auto padding = (8 - ( pos & 7 )) & 7;
    std::array<char, 8> padding_data = {0,0,0,0,0,0,0};
    std::uint64_t msg_len = data.size();

    file_.write(padding_data.data(), padding);
    file_.write(reinterpret_cast<char*>(&msg_len), sizeof(msg_len));
    file_.write(reinterpret_cast<char*>(data.data()), msg_len);
    file_.flush();
  });
}
}  // namespace aom