#ifndef AOM_DATALOG_GRPC_EXPORTER_H
#define AOM_DATALOG_GRPC_EXPORTER_H

#include "aom/datalog/storage_interface.h"
#include "slt/concur/work_pool.h"
#include "cogment/api/data.egrpc.pb.h"

#include "easy_grpc/easy_grpc.h"

#include <fstream>
#include <memory>

namespace aom {

class Grpc_datalog_exporter_base : public Datalog_storage_interface {
public:
  class Trial_log : public Trial_log_interface {
   public:
    Trial_log(Grpc_datalog_exporter_base* owner, const uuids::uuid& trial_id);
    ~Trial_log();

    void add_sample(cogment::DatalogSample data) override;
    void add_samples(std::vector<cogment::DatalogSample>&& data) override;
   
   private:
    Grpc_datalog_exporter_base* owner_;
  };

  void send(cogment::DatalogSample sample);
  void send(std::vector<cogment::DatalogSample>&& samples);

  std::unique_ptr<Trial_log_interface> begin_trial(
      const uuids::uuid& trial_id) final override;

  void set_stub(cogment::LogExporter::Stub_interface* stub) {stub_ = stub; }

private:
  cogment::LogExporter::Stub_interface* stub_ = nullptr;
};

// Stores Data samples to a local CVS file.
class Grpc_datalog_exporter : public Grpc_datalog_exporter_base {
 public:
  Grpc_datalog_exporter(const std::string& url);

 private:
   easy_grpc::Completion_queue work_thread;
   easy_grpc::client::Unsecure_channel channel;
   cogment::LogExporter::Stub stub_impl;
};
}  // namespace aom
#endif