#ifndef AOM_DATALOG_STORAGE_CVS_H
#define AOM_DATALOG_STORAGE_CVS_H

#include "aom/datalog/storage_interface.h"
#include "slt/concur/work_pool.h"

#include <fstream>
#include <memory>

namespace aom {

// Stores Data samples to a local CVS file.
class Local_csv_storage : public Datalog_storage_interface {
 public:
  class Trial_log : public Trial_log_interface {
   public:
    Trial_log(Local_csv_storage* owner, const uuids::uuid& trial_id);
    ~Trial_log();

    void add_sample(cogment::DatalogSample data) override;

   private:
    Local_csv_storage* owner_;
  };

  Local_csv_storage(const std::string& file_name);
  ~Local_csv_storage();
  std::unique_ptr<Trial_log_interface> begin_trial(
      const uuids::uuid& trial_id) final override;

  void send(cogment::DatalogSample sample);

 private:
  std::ofstream file_;

  slt::concur::Work_pool flush_pool;
};
}  // namespace aom
#endif