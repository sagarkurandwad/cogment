#!/bin/bash

# Go to root of aom-framework
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR/..

# Build the orchestrator's build environment
make cogment-cpp-builder

# Run the 
docker run -it --rm \
  -v $(pwd):/app \
  --user $(id -u ${USER}):$(id -g ${USER}) \
  --cap-add=SYS_PTRACE --security-opt seccomp=unconfined \
  -e TRIAL_SERVER_PORT=9000 \
  -e PROMETHEUS_PORT=8080 -p 8080:8080 \
  --network host \
  registry.gitlab.com/cogment/cogment:cpp_builder bash