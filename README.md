# Cogment Framework
The Cogment framework is a high-efficiency, open source framework designed to enable the training of models in environments where humans and agents interact with the environment and each other continuously. It’s capable of distributed, multi-agent, multi-model training.

| Homepage 	| [cogment.ai][1]       	|
|----------	|-------------------	|
| Gitter    | [https://gitter.im/cogment/community][2] 	|

## To start using the Cogment Framework
### Glossary and first steps
Before diving right in, we recommend taking the time to read our [glossary][3], which details the terminology we use for several critical concepts of the Cogment Framework.
You can also find how to [install][4] the framework in our [documentation][5].
Last but not least, a [tutorial][6] is available.

### Download cogment-cli
    
The latest cogment-cli is available at [releases](https://gitlab.com/cogment/cogment/-/releases).
Once downloaded you can list all commands

    cogment help

## To start developing for the Cogment Framework
Contributions are welcome!

Please read how to [contribute][7] which will guide you through the entire workflow of how to build the source code, how to run the tests, and how to contribute changes to the Cogment codebase. The "How to contribute" document also contains info on how the contribution process works and contains best practices for creating contributions.
### Rebuilding protocol buffers

Simply running `make protos` should take care of everything.
### Running tests of Python SDK

	cd sdk_python
	docker run --rm -v ${PWD}:/app sdk_py:latest python -m pytest

## Troubleshooting
If you have trouble using the framework, feel free to drop in by our [slack](cogment.slack.com) channel.

## About this repository
This repository contains the source code for the Cogment Framework.
(Add details on organization etc.)

[1]:	https://www.cogment.ai
[2]:	https://gitter.im/cogment/community
[3]:	/docs/glossary.md
[4]:	/docs/installation.md
[5]:	/docs/
[6]:	/docs/tutorial/
[7]:	/contributing.md