import cog_settings as settings

import pandas as pd
import time
import os
import base64
# import logging
import grpc
import traceback

from version import VERSION
from distutils.util import strtobool
from grpc_reflection.v1alpha import reflection
from concurrent import futures
from cogment.api.data_pb2 import LogReply, _LOGEXPORTER
from cogment.api.data_pb2_grpc import LogExporterServicer, add_LogExporterServicer_to_server
from cogment.delta_encoding import DecodeObservationData
from google.protobuf.json_format import MessageToJson

from sqlalchemy import create_engine

ENABLE_REFLECTION_VAR_NAME = 'COGMENT_GRPC_REFLECTION'
_ONE_DAY_IN_SECONDS = 60 * 60 * 24
TABLE_NAME = 'stats'

engine = create_engine(os.getenv('POSTGRES_ENGINE'), echo=False)


class LogExporterService(LogExporterServicer):
    """Provides methods that implement functionality of log exporter server."""

    def __init__(self):

        self.COLUMNS = ["trial_id", "tick_id", "timestamp"]
        self.last_obs = []
        for actor_class, count in settings.environment.actors:
            cols_obs = [
                x.name for x in actor_class.observation_space.DESCRIPTOR.fields]
            cols_actions = [
                x.name for x in actor_class.action_space.DESCRIPTOR.fields]
            cols_rewards = ["value", "confidence"]
            self.last_obs.extend([None] * count)
            for j in range(count):
                name = f"{actor_class.name}_{j}"
                obs_title = [f"{name}_observation_{x}" for x in cols_obs]
                action_title = [f"{name}_action_{x}" for x in cols_actions]
                reward_title = [f"{name}_reward_{x}" for x in cols_rewards]
                self.COLUMNS.extend(obs_title)
                self.COLUMNS.extend(action_title)
                self.COLUMNS.extend(reward_title)

    @staticmethod
    def convert_value(value, field_descriptor):
        if field_descriptor.type == field_descriptor.TYPE_BYTES:
            value = base64.b64encode(value).decode("utf-8")

        elif field_descriptor.type == field_descriptor.TYPE_MESSAGE:
            value = MessageToJson(value,  including_default_value_fields=True)

        #     if descriptor.label == descriptor.LABEL_REPEATED:
        #         map(dump_object, value)
        #     else:
        #         dump_object(value)

        elif field_descriptor.type == field_descriptor.TYPE_ENUM:
            value = field_descriptor.enum_type.values[value].name

        return value

    @staticmethod
    def get_value(obj, field_descriptor):
        value = getattr(obj, field_descriptor.name)
        if field_descriptor.label == field_descriptor.LABEL_REPEATED:
            return [LogExporterService.convert_value(x, field_descriptor) for x in value]

        return LogExporterService.convert_value(value, field_descriptor)

    def Log(self, request_iterator, context):
        try:
            connection = engine.connect()

            for sample in request_iterator:
                # print(MessageToJson(sample, True))

                row = [
                    sample.trial_id,
                    sample.observations.tick_id,
                    sample.observations.timestamp.ToNanoseconds() / 1000000000
                ]

                actor_id = 0
                for actor_class, count in settings.environment.actors:
                    # print(count)
                    for j in range(count):
                        # print(j)
                        try:
                            obs_id = sample.observations.actors_map[actor_id]
                        except Exception:
                            print(sample)

                        obs_data = sample.observations.observations[obs_id]
                        action_data = sample.actions[actor_id].content

                        obs = DecodeObservationData(
                            actor_class, obs_data, self.last_obs[actor_id])
                        self.last_obs[actor_id] = obs

                        for descriptor in actor_class.observation_space.DESCRIPTOR.fields:
                            value = self.get_value(obs, descriptor)
                            row.append(value)

                        for descriptor in actor_class.action_space.DESCRIPTOR.fields:
                            action = actor_class.action_space()
                            action.ParseFromString(action_data)

                            value = self.get_value(action, descriptor)
                            row.append(value)

                        row.append(sample.rewards[actor_id].value)
                        row.append(sample.rewards[actor_id].confidence)

                        actor_id += 1

                df = pd.DataFrame([row], columns=self.COLUMNS)

                df.to_sql(TABLE_NAME, connection, if_exists='append')

        except Exception as e:
            print("error:", str(e))
            traceback.print_exc()
            raise
        finally:
            if connection:
                connection.close()

        return LogReply()


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(thread_name_prefix="log_exporter"))
    add_LogExporterServicer_to_server(LogExporterService(), server)

    # Enable grpc reflection if requested
    if strtobool(os.getenv(ENABLE_REFLECTION_VAR_NAME, 'false')):
        SERVICE_NAMES = (_LOGEXPORTER.full_name, reflection.SERVICE_NAME,)

        reflection.enable_server_reflection(SERVICE_NAMES, server)

    server.add_insecure_port('[::]:9000')
    server.start()
    print('server started on port 9000')
    print(f'Log exporter version : {VERSION}')

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    # logging.basicConfig()
    serve()
