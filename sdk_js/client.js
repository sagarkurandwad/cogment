var services = require('./cogment/api/orchestrator_grpc_web_pb.js');
var protos = require('./cogment/api/orchestrator_pb.js');
var env_protos = require('./cogment/api/orchestrator_pb.js');
var common_protos = require('./cogment/api/common_pb.js');
var trial_module = require('./trial.js');
var delta_encoding = require('./delta_encoding.js');

class ClientTrial extends trial_module.Trial {
  constructor(conn, trial_start_rep, settings, actor_class, initial_observation, actor_counts) {
    super(trial_start_rep.getTrialId(), settings, actor_counts);

    this.connection = conn;
    this.actor_class = actor_class;
    this.observation = initial_observation;
    this.actor_id = trial_start_rep.getActorId();
  }

  async do_action(action) {
    this.flush_feedback();

    let act_req = new protos.TrialActionRequest();
    act_req.setTrialId(this.id);
    act_req.setActorId(this.actor_id);
    
    let act = new common_protos.Action();

    act.setContent(action.serializeBinary());
    act_req.setAction(act);

    return new Promise( (resolve, reject) => {
      this.connection.client.action(act_req, {}, (err, resp) => {
        if(err) {
          reject(new Error(`Trial action failure: [${err.code}] ${err.message}`));
        }
        else {
          this.observation = delta_encoding.decode_observation_data(this.actor_class, resp.getObservation().getData(), this.observation);
          this.tick_id = resp.getObservation().getTickId();
          resolve(this.observation);
        }
      });
    });
  }

  end() {
    this.flush_feedback();

    let end_req = new protos.TrialEndRequest();
    end_req.setTrialId(this.id);

    return new Promise( (resolve, _) => {
      this.connection.client.end(end_req, {}, (err, resp) => {
        resolve();
      });
    });
  }

  flush_feedback() {
    let feedbacks = this._get_all_feedback();

    if( feedbacks.length > 0) {
        let req = new protos.TrialFeedbackRequest();
        req.setTrialId(this.id);

        req.setFeedbacksList(feedbacks);
        this.connection.client.giveFeedback(req, {}, (_, __) => {});
    }
  }
}

class Connection {

  constructor(settings, endpoint) {
    this.client = new services.TrialClient(endpoint);
    this.settings = settings;
  }

  async start_trial(actor_class, cfg=undefined) {
    return new Promise((resolve, reject) => {
      var start_req = new protos.TrialStartRequest();   
      if(cfg) {
        var cfg_msg = new env_protos.TrialConfig()
        cfg_msg.setContent(cfg.serializeBinary());
        start_req.setConfig(cfg_msg);
      }

      this.client.start(start_req, {}, (err, resp) => {
        if(err) {
          reject(new Error(`Trial start failure: [${err.code}] ${err.message}`));
        }
        else {
          var observation = delta_encoding.decode_observation_data(actor_class, resp.getObservation().getData());
          
          var trial = new ClientTrial(this, resp, this.settings, actor_class, observation);
          resolve(trial);
        }
      });
    });
  }
}

module.exports.Connection = Connection;