/**
 * @fileoverview gRPC-Web generated client stub for cogment
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var cogment_api_agent_pb = require('../../cogment/api/agent_pb.js')

var cogment_api_environment_pb = require('../../cogment/api/environment_pb.js')
const proto = {};
proto.cogment = require('./supervisor_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.TrialSupervisorClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.TrialSupervisorPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.TrialConfigRequest,
 *   !proto.cogment.TrialConfigReply>}
 */
const methodInfo_TrialSupervisor_Configure = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.TrialConfigReply,
  /** @param {!proto.cogment.TrialConfigRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.TrialConfigReply.deserializeBinary
);


/**
 * @param {!proto.cogment.TrialConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.TrialConfigReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.TrialConfigReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialSupervisorClient.prototype.configure =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.TrialSupervisor/Configure',
      request,
      metadata || {},
      methodInfo_TrialSupervisor_Configure,
      callback);
};


/**
 * @param {!proto.cogment.TrialConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.TrialConfigReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialSupervisorPromiseClient.prototype.configure =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.TrialSupervisor/Configure',
      request,
      metadata || {},
      methodInfo_TrialSupervisor_Configure);
};


module.exports = proto.cogment;

