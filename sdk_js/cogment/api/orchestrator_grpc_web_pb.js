/**
 * @fileoverview gRPC-Web generated client stub for cogment
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var cogment_api_common_pb = require('../../cogment/api/common_pb.js')
const proto = {};
proto.cogment = require('./orchestrator_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.TrialClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.TrialPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.TrialStartRequest,
 *   !proto.cogment.TrialStartReply>}
 */
const methodInfo_Trial_Start = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.TrialStartReply,
  /** @param {!proto.cogment.TrialStartRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.TrialStartReply.deserializeBinary
);


/**
 * @param {!proto.cogment.TrialStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.TrialStartReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.TrialStartReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialClient.prototype.start =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Trial/Start',
      request,
      metadata || {},
      methodInfo_Trial_Start,
      callback);
};


/**
 * @param {!proto.cogment.TrialStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.TrialStartReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialPromiseClient.prototype.start =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Trial/Start',
      request,
      metadata || {},
      methodInfo_Trial_Start);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.TrialEndRequest,
 *   !proto.cogment.TrialEndReply>}
 */
const methodInfo_Trial_End = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.TrialEndReply,
  /** @param {!proto.cogment.TrialEndRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.TrialEndReply.deserializeBinary
);


/**
 * @param {!proto.cogment.TrialEndRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.TrialEndReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.TrialEndReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialClient.prototype.end =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Trial/End',
      request,
      metadata || {},
      methodInfo_Trial_End,
      callback);
};


/**
 * @param {!proto.cogment.TrialEndRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.TrialEndReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialPromiseClient.prototype.end =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Trial/End',
      request,
      metadata || {},
      methodInfo_Trial_End);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.TrialActionRequest,
 *   !proto.cogment.TrialActionReply>}
 */
const methodInfo_Trial_Action = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.TrialActionReply,
  /** @param {!proto.cogment.TrialActionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.TrialActionReply.deserializeBinary
);


/**
 * @param {!proto.cogment.TrialActionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.TrialActionReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.TrialActionReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialClient.prototype.action =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Trial/Action',
      request,
      metadata || {},
      methodInfo_Trial_Action,
      callback);
};


/**
 * @param {!proto.cogment.TrialActionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.TrialActionReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialPromiseClient.prototype.action =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Trial/Action',
      request,
      metadata || {},
      methodInfo_Trial_Action);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.TrialHeartbeatRequest,
 *   !proto.cogment.TrialHeartbeatReply>}
 */
const methodInfo_Trial_Heartbeat = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.TrialHeartbeatReply,
  /** @param {!proto.cogment.TrialHeartbeatRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.TrialHeartbeatReply.deserializeBinary
);


/**
 * @param {!proto.cogment.TrialHeartbeatRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.TrialHeartbeatReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.TrialHeartbeatReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialClient.prototype.heartbeat =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Trial/Heartbeat',
      request,
      metadata || {},
      methodInfo_Trial_Heartbeat,
      callback);
};


/**
 * @param {!proto.cogment.TrialHeartbeatRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.TrialHeartbeatReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialPromiseClient.prototype.heartbeat =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Trial/Heartbeat',
      request,
      metadata || {},
      methodInfo_Trial_Heartbeat);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.TrialFeedbackRequest,
 *   !proto.cogment.TrialFeedbackReply>}
 */
const methodInfo_Trial_GiveFeedback = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.TrialFeedbackReply,
  /** @param {!proto.cogment.TrialFeedbackRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.TrialFeedbackReply.deserializeBinary
);


/**
 * @param {!proto.cogment.TrialFeedbackRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.TrialFeedbackReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.TrialFeedbackReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialClient.prototype.giveFeedback =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Trial/GiveFeedback',
      request,
      metadata || {},
      methodInfo_Trial_GiveFeedback,
      callback);
};


/**
 * @param {!proto.cogment.TrialFeedbackRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.TrialFeedbackReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialPromiseClient.prototype.giveFeedback =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Trial/GiveFeedback',
      request,
      metadata || {},
      methodInfo_Trial_GiveFeedback);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.VersionRequest,
 *   !proto.cogment.VersionInfo>}
 */
const methodInfo_Trial_Version = new grpc.web.AbstractClientBase.MethodInfo(
  cogment_api_common_pb.VersionInfo,
  /** @param {!proto.cogment.VersionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  cogment_api_common_pb.VersionInfo.deserializeBinary
);


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.VersionInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.VersionInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialClient.prototype.version =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Trial/Version',
      request,
      metadata || {},
      methodInfo_Trial_Version,
      callback);
};


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.VersionInfo>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialPromiseClient.prototype.version =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Trial/Version',
      request,
      metadata || {},
      methodInfo_Trial_Version);
};


module.exports = proto.cogment;

