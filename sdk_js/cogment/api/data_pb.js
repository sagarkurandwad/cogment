/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var cogment_api_common_pb = require('../../cogment/api/common_pb.js');
var cogment_api_agent_pb = require('../../cogment/api/agent_pb.js');
var cogment_api_environment_pb = require('../../cogment/api/environment_pb.js');
goog.exportSymbol('proto.cogment.DatalogSample', null, global);
goog.exportSymbol('proto.cogment.LogReply', null, global);

/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.cogment.DatalogSample = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.cogment.DatalogSample.repeatedFields_, null);
};
goog.inherits(proto.cogment.DatalogSample, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.cogment.DatalogSample.displayName = 'proto.cogment.DatalogSample';
}
/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.cogment.DatalogSample.repeatedFields_ = [3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.cogment.DatalogSample.prototype.toObject = function(opt_includeInstance) {
  return proto.cogment.DatalogSample.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.cogment.DatalogSample} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.cogment.DatalogSample.toObject = function(includeInstance, msg) {
  var f, obj = {
    trialId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    userId: jspb.Message.getFieldWithDefault(msg, 5, ""),
    observations: (f = msg.getObservations()) && cogment_api_environment_pb.ObservationSet.toObject(includeInstance, f),
    actionsList: jspb.Message.toObjectList(msg.getActionsList(),
    cogment_api_common_pb.Action.toObject, includeInstance),
    rewardsList: jspb.Message.toObjectList(msg.getRewardsList(),
    cogment_api_agent_pb.Reward.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.cogment.DatalogSample}
 */
proto.cogment.DatalogSample.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.cogment.DatalogSample;
  return proto.cogment.DatalogSample.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.cogment.DatalogSample} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.cogment.DatalogSample}
 */
proto.cogment.DatalogSample.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setTrialId(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = new cogment_api_environment_pb.ObservationSet;
      reader.readMessage(value,cogment_api_environment_pb.ObservationSet.deserializeBinaryFromReader);
      msg.setObservations(value);
      break;
    case 3:
      var value = new cogment_api_common_pb.Action;
      reader.readMessage(value,cogment_api_common_pb.Action.deserializeBinaryFromReader);
      msg.addActions(value);
      break;
    case 4:
      var value = new cogment_api_agent_pb.Reward;
      reader.readMessage(value,cogment_api_agent_pb.Reward.deserializeBinaryFromReader);
      msg.addRewards(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.cogment.DatalogSample.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.cogment.DatalogSample.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.cogment.DatalogSample} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.cogment.DatalogSample.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTrialId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getObservations();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      cogment_api_environment_pb.ObservationSet.serializeBinaryToWriter
    );
  }
  f = message.getActionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      cogment_api_common_pb.Action.serializeBinaryToWriter
    );
  }
  f = message.getRewardsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      cogment_api_agent_pb.Reward.serializeBinaryToWriter
    );
  }
};


/**
 * optional string trial_id = 1;
 * @return {string}
 */
proto.cogment.DatalogSample.prototype.getTrialId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/** @param {string} value */
proto.cogment.DatalogSample.prototype.setTrialId = function(value) {
  jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string user_id = 5;
 * @return {string}
 */
proto.cogment.DatalogSample.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/** @param {string} value */
proto.cogment.DatalogSample.prototype.setUserId = function(value) {
  jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional ObservationSet observations = 2;
 * @return {?proto.cogment.ObservationSet}
 */
proto.cogment.DatalogSample.prototype.getObservations = function() {
  return /** @type{?proto.cogment.ObservationSet} */ (
    jspb.Message.getWrapperField(this, cogment_api_environment_pb.ObservationSet, 2));
};


/** @param {?proto.cogment.ObservationSet|undefined} value */
proto.cogment.DatalogSample.prototype.setObservations = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.cogment.DatalogSample.prototype.clearObservations = function() {
  this.setObservations(undefined);
};


/**
 * Returns whether this field is set.
 * @return {!boolean}
 */
proto.cogment.DatalogSample.prototype.hasObservations = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * repeated Action actions = 3;
 * @return {!Array<!proto.cogment.Action>}
 */
proto.cogment.DatalogSample.prototype.getActionsList = function() {
  return /** @type{!Array<!proto.cogment.Action>} */ (
    jspb.Message.getRepeatedWrapperField(this, cogment_api_common_pb.Action, 3));
};


/** @param {!Array<!proto.cogment.Action>} value */
proto.cogment.DatalogSample.prototype.setActionsList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.cogment.Action=} opt_value
 * @param {number=} opt_index
 * @return {!proto.cogment.Action}
 */
proto.cogment.DatalogSample.prototype.addActions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.cogment.Action, opt_index);
};


proto.cogment.DatalogSample.prototype.clearActionsList = function() {
  this.setActionsList([]);
};


/**
 * repeated Reward rewards = 4;
 * @return {!Array<!proto.cogment.Reward>}
 */
proto.cogment.DatalogSample.prototype.getRewardsList = function() {
  return /** @type{!Array<!proto.cogment.Reward>} */ (
    jspb.Message.getRepeatedWrapperField(this, cogment_api_agent_pb.Reward, 4));
};


/** @param {!Array<!proto.cogment.Reward>} value */
proto.cogment.DatalogSample.prototype.setRewardsList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.cogment.Reward=} opt_value
 * @param {number=} opt_index
 * @return {!proto.cogment.Reward}
 */
proto.cogment.DatalogSample.prototype.addRewards = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.cogment.Reward, opt_index);
};


proto.cogment.DatalogSample.prototype.clearRewardsList = function() {
  this.setRewardsList([]);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.cogment.LogReply = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.cogment.LogReply, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.cogment.LogReply.displayName = 'proto.cogment.LogReply';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.cogment.LogReply.prototype.toObject = function(opt_includeInstance) {
  return proto.cogment.LogReply.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.cogment.LogReply} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.cogment.LogReply.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.cogment.LogReply}
 */
proto.cogment.LogReply.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.cogment.LogReply;
  return proto.cogment.LogReply.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.cogment.LogReply} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.cogment.LogReply}
 */
proto.cogment.LogReply.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.cogment.LogReply.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.cogment.LogReply.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.cogment.LogReply} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.cogment.LogReply.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


goog.object.extend(exports, proto.cogment);
