var common_pb = require('./cogment/api/common_pb.js');
var orch_pb = require('./cogment/api/orchestrator_pb.js');

/*
class Actor:
    def __init__(self, actor_class, actor_id):
        self.actor_class = actor_class
        self.actor_id = actor_id
        self._feedback = []

    def add_feedback(self, time, value, confidence, user_data=None):
        self._feedback.append((time, value, confidence, user_data))
*/
class Actor {
  constructor(actor_class, actor_id, trial) {
    this.actor_class = actor_class;
    this.actor_id = actor_id;
    this.feedbacks = []
    this.trial = trial
  }

  add_feedback(value, confidence, time=undefined) {
    if(time === undefined) {
      time = -1;
    }
    this.feedbacks.push({
      time:time,
      value:value,
      confidence: confidence
    });
  }
  
}

class Trial {
  constructor(id, settings, actor_counts) {
    this.id = id;
    this.settings = settings;
    this.actors = {};
    this.actors.all = [];
    this.tick_id = 0;
    
    let actor_id = 0;

    let a_c_names = Object.keys(settings.actor_classes);

    for(var i in actor_counts) {
      let actor_list = [];
      let a_c = settings.actor_classes[a_c_names[i]];
      let count = actor_counts[i];

      for(var j = 0 ; j < count; ++j) {
        let act = new Actor(a_c, actor_id, this);
        actor_list.push(act);
        this.actors.all.push(act)
        ++actor_id;
      }
      
      this.actors[c.actor_class.id] = actor_list
    }
  }

  _get_all_feedback() {
    let result = []
 
    for(const actor of this.actors.all) {
      for(const src_fb of actor.feedbacks) {
        let fb = new common_pb.Feedback();
        fb.setActorId(actor.actor_id);
        fb.setTickId(src_fb.time);
        fb.setValue(src_fb.value);
        fb.setConfidence(src_fb.confidence);

        result.push(fb);
      }

      actor.feedbacks = [];
    }

    return result;
  }
}

module.exports.Trial = Trial;
