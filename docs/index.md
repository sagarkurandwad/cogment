

# Cogment Framework
[![Gitter](https://badges.gitter.im/cogment/community.svg)](https://gitter.im/cogment/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

The Cogment framework is a high-efficiency, open source framework designed to enable the training of models in environments where humans and agents interact with the environment and each other continuously. It’s capable of distributed, multi-agent, multi-model training.

| Homepage 	| [cogment.ai][1]       	|
|----------	|-------------------	|
| Slack    	| [cogment.slack.com][2] 	|

## Installation

In order to use cogment, you will need to have [Docker](https://docs.docker.com/install/) installed.

Beyond that, the easiest way to get started is to use the `cogment-cli` command line tool. Download the latest version 
of [cogment-cli](https://gitlab.com/cogment/cogment/-/releases) for your environment.


## Glossary and first steps
Before diving right in, we recommend taking the time to read our [glossary][3], which details the terminology we use for several critical concepts of the Cogment Framework.
You can also find how to [install][4] the framework in our [documentation][5].
Last but not least, a [tutorial][6] is available.

## Troubleshooting
Feel free to ping us on [gitter](https://gitter.im/cogment/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link) if you have any questions!

[1]:	https://cogment.ai
[2]:	https://cogment.slack.com
[3]:	/glossary
[4]:	/installation
[5]:	/
[6]:	/tutorial/intro