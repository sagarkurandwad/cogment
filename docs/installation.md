# Installation

In order to use cogment, you will need to have [Docker](https://docs.docker.com/install/) installed.

Beyond that, the easiest way to get started is to use the `cogment` command line tool.

## *On Unix-like environments:*
```
	docker pull registry.gitlab.com/cogment/cogment/cli:latest
	alias cogment-cli='docker run -u ${UID}:${UID} -ti -v ${PWD}:/app registry.gitlab.com/cogment/cogment/cli:latest'
```

## *On Windows:*
```
	docker pull registry.gitlab.com/cogment/cogment/cli:latest
```

and put the following in a `.bat` file:

```
docker run -ti -v %cd%:/app registry.gitlab.com/cogment/cogment/cli:latest %*
```
