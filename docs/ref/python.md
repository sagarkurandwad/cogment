# Python SDK

## Installation

The simplest way to install the python SDK is to just install it using pip:
`pip install cogment`

## General usage

### cog_settings.py
All API entrypoints require a cogment configuration object. This configuration object can be deterministically determined
from the content of a project's `cogment.yaml`. As such, it should be generated using the `cogment-cli` tool.

```bash
# From the directory containing cogment.yaml
$ cogment-cli generate --python_dir=path/to/js/project
```

This will generate both a `cog_settings.py` file, as well as any required compiled protocol buffer files.

### Top-level import

Wether a script implements a client, agent or environment, it should import both the `cog_settings` and cogment itself.

```python
import cog_settings
import cogment
```

## Common API

### ```class cogment.Actor```

Represents an actor within a trial.

**Members:**

```def add_feedback(self, value, confidence, tick_id=-1, user_data=None)```

Provide feedback that will contribute to that actor's reward.

parameters:

- `value`: The numerical value of the feedback
- `confidence`: The weight this feedback has relative to other feedbacks.
- `time`: The tick id for which this feedback refers to. If left at -1, it implicitely refers to the latest point in time, and can be **reliably** expected to be propagated live to the agent.
- `user_data`: consider unused for now.

### ```class cogment.Trial```

**Members:**

- `id`: The trial id.
- `settings`: The settings object associated with this trial.
- `actors`: A simple namespace of actor classes. Each entry is a list of actors.
- `actors.all`: A linear list of all actors present in the trial. 
- `tick_id`: The current tick id.

Represents an actor within a trial.

**Members:**

```def add_feedback(self, value, confidence, tick_id=-1, user_data=None)```

Provide feedback that will contribute to that actor's reward.

parameters:

- `value`: The numerical value of the feedback
- `confidence`: The weight this feedback has relative to other feedbacks.
- `time`: The tick id for which this feedback refers to. If left at -1, it implicitely refers to the latest point in time, and can be **reliably** expected to be propagated live to the agent.
- `user_data`: consider unused for now.


## Client API

### ```class cogment.client.Connection```

This class creates a connection to a cogment orchestrator living at **endpoint**. The first parameter should be 
the `cog_settings` module of the project.

**Members:**

```def __init__(self, cog_settings, endpoint)```

parameters:

- `cog_settings`: The cog_settings **module** of the project.
- `endpoint`: A string containing the url of the project's Orchestrator.  

```def start_trial(self, actor_class, env_cfg=None)```

Begins a new trial

parameters:

- `actor_class`: The actor class the human is registering as
- `env_cfg`: if applicable, an instance of the environment's config protocol buffer.
  
returns:
- An instance of `ClientTrial` upon successfull trial creation.


### ```class cogment.client.ClientTrial(cogment.Trial)```

An active trial from the perspective of a client application.

**Members:**

```def do_action(self, action)```
Request the advancement of time for the trial by providing a human action. Any pending feedback is also flushed.

parameters:

- `action`: the human's action, must be an instance of the client's actor class' action space.

returns:

- The updated Observation for the local actor. The promised object is the same as `ClientTrial.observation`.

```def end(self)```

Request the end of the trial. Any pending feedback is also flushed.

```def flush_feedback(self)```

Explicitely flush any pending feedback. This should rarely be needed.


## Agent API

### ```class cogment.Agent```

This is expected to be subclassed by project-specific agent inmplementations.

**Members:**

```VERSIONS``` 

Class variable containing a dictionary of strings representing version information.

```actor_class```

Class variable containing a reference to the actor class implemented by this agent.

```def __init__(self, trial)```

This is invoked at the start of a trial.

```def end(self)```

This is invoked at the end of the trial.

```def reward(self, reward)```

This is invoked when reward information is available.
    
```def decide(self, observation)```

This is expected to return an action from the agent's action space.

## Environment API

### ```class cogment.Environemnt```

This is expected to be subclassed by project-specific agent inmplementations

**Members:**

```VERSIONS``` 

Class variable containing a dictionary of strings representing version information.

```def start(self, config)```

This is invoked at the start of a trial.

```def update(self, actions)```

This is expected to return an observation set in response to a set of actions from all actors.

```def end(self)```

This is invoked at the end of the trial.

## Service API

### ```class cogment.GrpcServer```

```def __init__(self, service_type, settings, port=DEFAULT_PORT)```

parameters:

- `service_type`: Either a subclass of `cogment.Agent` or `cogment.Environment`.
- `settings`: The cog_settings **module** of the project.
- `port`: The port at which the server runs.

```def start(self)```

Starts the server

```def stop(self)```

Stops the server

```def serve(self)```

Serves the server, and block until a signal is received.