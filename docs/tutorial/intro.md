# Introduction

The following tutorial provides a concrete example of how to use the Cogment framework.  The classic child's game, Rock-Paper-Scissors (RPS), will be used to illustrate the setting up of a fully distributed ecosystem for training a model that includes a human.
